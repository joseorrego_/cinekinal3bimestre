package edu.kinal.org.peliculakinal;

import android.content.ContentValues;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.ActionBarActivity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PeliculasFragment extends Fragment {
    private ListView ListaPeliculasC;

    public SQLiteDatabase db;
    public DBSqlite sqlite;

    //public ArrayList<Peliculas> listaPelicula = new ArrayList<>();

    //private Peliculas [] datosTitular = new Peliculas[]{
    //        new Peliculas ("El Hombre Araña", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Harry Potter y las reliquias de la muerte (Parte 2)", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Los juegos del hambre", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Grandes Heroes", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Rapido y Furioso 7", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Home", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Divergente", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Bajo la misma estrella", "Excelente pelicula", "comentario"),
    //      new Peliculas ("50 Sombras de Grey", "Excelente pelicula", "comentario"),
    //      new Peliculas ("Monster inc", "Excelente pelicula", "comentario"),
    //};

    public PeliculasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_peliculas, container, false);

        ListaPeliculasC = (ListView)view.findViewById(R.id.listaPeliculas);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.7/CineKinal2009190/public").build();
        PeliculaService service = restAdapter.create(PeliculaService.class);

        service.getPeliculas(new Callback<List<Peliculas>>() {


            @Override
            public void success(List<Peliculas> peliculases, Response response) {
                AdapatorPeliculas adaptador = new AdapatorPeliculas(getActivity(), peliculases);
                ListaPeliculasC.setAdapter(adaptador);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(),"Error: "+ error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        if (v.getId() == R.id.listapeliculas){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Peliculas titularMenu = (Peliculas)ListaPeliculasC.getAdapter().getItem(info.position);
            menu.setHeaderTitle(titularMenu.getTitulo());
            inflater.inflate(R.menu.menu_ctx_lista,menu);

        }
    }

    public class AdapatorPeliculas extends ArrayAdapter<Peliculas>{
        private List<Peliculas> lPeliculas;

        public AdapatorPeliculas(Context context, List<Peliculas> peliculas){
            super(context, R.layout.listitem_peliculas, peliculas);
            lPeliculas = peliculas;
        }

        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.listitem_peliculas, null);
            TextView ltitulo = (TextView)item.findViewById(R.id.titulo);
            ltitulo.setText(lPeliculas.get(position).getTitulo());

            TextView lsinopsis = (TextView)item.findViewById(R.id.sinopsis);
            lsinopsis.setText(lPeliculas.get(position).getSinopsis());

            return item;
        }
    }

    //@Override
    //public boolean onContextItemSelected(MenuItem item){
      //  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
      //  switch (item.getItemId()){
      //      case R.id.CtxListTitularFavorito:
       //         sqlite = new DBSqlite(getActivity().getBaseContext());
        //        db = sqlite.getReadableDatabase();
//
           //     String Sql = "UPDATE PELICULAS SET favorito='1' WHERE Titulo='"+lPeliculas.get(info.position).getTitulo().toString()+"'";
          //      db.execSQL(Sql);
            //    db.close();

            //    Toast.makeText(getActivity().getApplicationContext(), "Agreado a favoritos"+info.position, Toast.LENGTH_LONG).show();
            //default:
             //   return super.onContextItemSelected(item);
       // }
    //}

}