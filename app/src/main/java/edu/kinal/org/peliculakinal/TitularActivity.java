package edu.kinal.org.peliculakinal;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.support.v4.app.NavUtils;


public class TitularActivity extends ActionBarActivity {

    private Toolbar mToolbar;
    private TextView tvTitulo;
    private TextView tvComentario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titular);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTitulo = (TextView)findViewById(R.id.tvTituloTitular);
        tvComentario = (TextView)findViewById(R.id.tvDescripcion);

        Bundle extras = getIntent().getExtras();
        tvTitulo.setText(extras.getString("Titulo"));
        tvComentario.setText(extras.getString("Subtitulo"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_titular, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_configuracion){
            Intent settings = new Intent(TitularActivity.this,SettingsActivity.class);
            startActivity(settings);
            return true;
        }if (id == R.id.action_settings) {
            Intent cerrarsesison = new Intent(TitularActivity.this,LoginActivity.class);
            startActivity(cerrarsesison);
            finish();
            return true;
        }if (id == R.id.action_perfil) {
            Intent perfil = new Intent(TitularActivity.this,PerfilActivity.class);
            startActivity(perfil);
            finish();
            return true;
        }
        if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}