package edu.kinal.org.peliculakinal;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class RegistroActivity extends ActionBarActivity {

    EditText Usuarionuevo,Contrasenanueva,Contrasenanuevaconfir;
    Button btningresar;
    RelativeLayout rl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        btningresar= (Button) findViewById(R.id.btningresar);
        Usuarionuevo = (EditText) findViewById(R.id.Usuarionuevo);
        Contrasenanueva = (EditText) findViewById(R.id.Contrasenanueva);
        Contrasenanuevaconfir = (EditText) findViewById(R.id.Contrasenanuevaconfir);
        Bundle miBundle = this.getIntent().getExtras();

        try{
            btningresar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (Usuarionuevo.getText().toString().matches("") || Contrasenanueva.getText().toString().matches("") || Contrasenanuevaconfir.getText().toString().matches("")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Ingrese correctamente los datos", Toast.LENGTH_LONG);
                        toast.show();

                    } else if (Contrasenanueva.getText().toString().matches(Contrasenanuevaconfir.getText().toString())) {
                        ManejadorUsuario.getInstancia().agregarUsuario(Usuarionuevo.getText().toString(),Contrasenanueva.getText().toString());

                        Toast t = Toast.makeText(getApplicationContext(),Usuarionuevo.getText().toString()+"Registrado!",Toast.LENGTH_SHORT);
                        Intent myIntent = new Intent(view.getContext(), MainActivity.class);

                        startActivityForResult(myIntent, 0);
                        t.show();
                        finish();


                    } else if (Contrasenanueva.getText().toString() != Contrasenanuevaconfir.getText().toString()) {
                        Toast t = Toast.makeText(getApplicationContext(), "La contrase�a no coincide", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            });
        }catch (NullPointerException e){

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registro, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}