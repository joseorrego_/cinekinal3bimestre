package edu.kinal.org.peliculakinal;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {

    ListView lista;

    public SQLiteDatabase db;
    public DBSqlite sqlite;

    public ArrayList<Peliculas> listaPelicula = new ArrayList<>();

    public FavoritosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favoritos, container, false);
        peliculasLista();

        lista = (ListView)view.findViewById(R.id.listapeliculasf);
        AdaptadorTitulares adaptadorTitulares = new AdaptadorTitulares(getActivity(), listaPelicula);
        lista.setAdapter(adaptadorTitulares);


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(getActivity(), TitularActivity.class);
                Peliculas titularSeleccionado = (Peliculas)parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", titularSeleccionado.getTitulo());
                extras.putString("Subtitulo", titularSeleccionado.getDescripcion());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });

        registerForContextMenu(lista);

        return view;
    }

    public void peliculasLista(){
        sqlite = new DBSqlite(getActivity().getBaseContext());
        db = sqlite.getReadableDatabase();

        String Sql = "SELECT TITULO,CONTENIDO FROM Peliculas where favorito = '1'";

        Cursor cc = db.rawQuery(Sql, null);
        int valida = 0;
        Peliculas obj;
        if (cc.moveToFirst()) {
            do {
                obj = new Peliculas();
                obj.setTitulo(cc.getString(0));
                obj.setDescripcion(cc.getString(1));
                listaPelicula.add(obj);
                valida++;
            } while (cc.moveToNext());
        }
        db.close();
    }

    class AdaptadorTitulares extends ArrayAdapter<Peliculas> {
        public AdaptadorTitulares (Context context, ArrayList<Peliculas> datos){
            super(context, R.layout.listitem_peliculas, datos);
        }
        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.listitem_peliculas, null);

            TextView tvTitulo = (TextView)item.findViewById(R.id.tvTitulo);
            TextView tvSubtitulo = (TextView)item.findViewById(R.id.tvSubtitulo);

            String tituloTitular = listaPelicula.get(position).getTitulo();
            String subtituloTitular = listaPelicula.get(position).getDescripcion();

            tvTitulo.setText(tituloTitular);
            tvSubtitulo.setText(subtituloTitular);
            return item;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu,v,menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        if (v.getId() == R.id.listapeliculasf){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Peliculas titularMenu = (Peliculas)lista.getAdapter().getItem(info.position);
            menu.setHeaderTitle(titularMenu.getTitulo());
            inflater.inflate(R.menu.menu_ctx_lista,menu);

        }
    }

}
