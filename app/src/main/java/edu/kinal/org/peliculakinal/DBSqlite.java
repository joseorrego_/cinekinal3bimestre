package edu.kinal.org.peliculakinal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


public class DBSqlite extends SQLiteOpenHelper{

    public static final String TABLE_ID = "_idPelicula";
    public static final String TITULO = "Titulo";
    public static final String CONTENIDO = "Contenido";

    public static final String FAVORITO = "favorito";

    private static final String DATABASE = "CineKinal";
    private static final String TABLE = "Peliculas";

    public DBSqlite(Context context) {
        super(context, DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE+" ("+TABLE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+TITULO+" TEXT,"+CONTENIDO+" TEXT,"+FAVORITO+" TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXITS "+TABLE);
        onCreate(db);
    }
}