package edu.kinal.org.peliculakinal;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener {

    private Toolbar mToolbar;

    int estado;
    private TabHost tabHost;
    private Resources re;

    public SQLiteDatabase db;
    public DBSqlite sqlite;

    private Peliculas [] datosTitularf = new Peliculas[100];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        re = getResources();

        agregarPeliculas();
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragmnet);

        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragmnet);
        drawerFragment.setDrawerListener(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        boolean notificacionsEnable = preferences.getBoolean("is_notification_enable",false);
        //Toast.makeText(this,"Notificaciones:"+notificacionsEnable, Toast.LENGTH_LONG).show();

        estado = 0;
        estadoactivo();
    }
    public void agregarPeliculas(){
        try {
            String TABLE = "Peliculas";
            sqlite = new DBSqlite(getBaseContext());
            db = sqlite.getReadableDatabase();

            String sql1 = "DELETE FROM Peliculas";
            db.execSQL(sql1);

            ContentValues valores = new ContentValues();
            valores.put("Titulo"," Tiempos Felices");
            valores.put("Contenido","Tiempos Felices es una comedia romántica que cuenta la historia de Max, un joven quien ha intentado todo para cortar con su novia pero no lo puede lograr.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Tomorrowland: El Mundo del mañana");
            valores.put("Contenido","Unidos por el mismo destino, un adolescente inteligente y optimista lleno de curiosidad científica y un antiguo niño prodigio inventor hastiado por las desilusiones se embarcan en una peligrosa misión para desenterrar los secretos de un enigmático lugar localizado en algún lugar del tiempo y el espacio conocido en la memoria colectiva como “Tomorrowland”.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Intensa-Mente");
            valores.put("Contenido","Hacerse mayor puede ser un camino lleno de obstáculos. También para Riley, que debe dejar su vida en el Medio Oeste cuando su padre consigue un nuevo trabajo en San Francisco. Como todos nosotros, Riley es guiada por sus emociones: Alegría, Miedo, Ira, Asco y Tristeza.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Los Vengadores: La era de Ultron");
            valores.put("Contenido","Cuando Tony Stark intenta reactivar un programa sin uso que tiene como objetivo de mantener la paz, las cosas comienzan a torcerse y los héroes más poderosos de la Tierra, incluyendo a Iron Man, Capitán América, Thor, El Increíble Hulk, Viuda Negra y Ojo de Halcón, se verán ante su prueba definitiva cuando el destino del planeta se ponga en juego.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","La Noche del Demonio 3");
            valores.put("Contenido","Elise Rainier (Lin Shaye) acepta a regañadientes utilizar su capacidad de ponerse en contacto con los muertos a fin de ayudar a una adolescente (Stefanie Scott) que se ha convertido en el blanco de una peligrosa entidad sobrenatural.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Mad Max: Furia en el Camino");
            valores.put("Contenido","Cuarta entrega de la saga post-apocalíptica que resucita la trilogía que a principios de los ochenta protagonizó Mel Gibson. Ahora es Tom Hardy quien interpreta a Max Rockatansky en una cinta en la que comparte estrellato con Charlize Theron, que da vida a la emperatriz Furiosa.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","The Gunman: El Objetivo");
            valores.put("Contenido","Un francotirador en un equipo de asesinos mercenarios, mata al ministro de minas del Congo. Exitoso disparo mortal de Terrier le obliga a esconderse. Volviendo a los años posteriores Congo, se convierte en el blanco de un mismo escuadrón.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Tomorrowland: El Mundo del mañana");
            valores.put("Contenido","Unidos por el mismo destino, un adolescente inteligente y optimista lleno de curiosidad científica y un antiguo niño prodigio inventor hastiado por las desilusiones se embarcan en una peligrosa misión para desenterrar los secretos de un enigmático lugar localizado en algún lugar del tiempo y el espacio conocido en la memoria colectiva como “Tomorrowland”.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);

            valores.clear();
            valores.put("Titulo","Dragon Ball Z: Resurreccion de Freezer");
            valores.put("Contenido","Después de que Bills, el Dios de la destrucción, decidiera no destruir la Tierra, se vive una gran época de paz. Hasta que Sorbet y Tagoma, antiguos miembros élite de la armada de Freezer, llegan a la Tierra con el objetivo de revivir a su líder por medio de las Bolas de Dragón.");
            valores.put("favorito","0");
            db.insert(TABLE,null,valores);
            db.close();
        } catch (SQLException e) {
            String message = e.toString();
            Toast.makeText(this, "0)" + message, Toast.LENGTH_LONG).show();
            db.close();
        }
    }


    public void estadoactivo(){
        if (estado == 0){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);

        }else if (estado == 1){

        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        //Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 1:
                fragment = new PeliculasFragment();
                title = "Pelicula";
                break;
            case 2:
                fragment = new FavoritosFragment();
                title = "Favoritos";
                break;
            default:
                break;
        }
        if (fragment != null){
            ((RelativeLayout)findViewById(R.id.ContainerLayout)).removeAllViewsInLayout();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ContainerLayout,fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       if (id == R.id.action_configuracion){
            Intent settings = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(settings);
            return true;
        }if (id == R.id.action_settings) {
            Intent cerrarsesison = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(cerrarsesison);
            finish();
            return true;
        }if (id == R.id.action_perfil) {
            Intent perfil = new Intent(MainActivity.this,PerfilActivity.class);
            startActivity(perfil);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            default:
                return super.onContextItemSelected(item);
        }
    }


}
