package edu.kinal.org.peliculakinal;

import java.util.ArrayList;
import java.util.List;

public class ManejadorUsuario {
    List<Usuario> Usuarios = new ArrayList<Usuario>();
    private static ManejadorUsuario instancia;


    public void agregarUsuario(String nombreUsuario,String pass){
        Usuarios.add(new Usuario(nombreUsuario,pass));
    }

    public List<Usuario> obtenerUsuarios(){
        return this.Usuarios;
    }
    public static ManejadorUsuario getInstancia(){
        if(instancia==null)
            instancia=new ManejadorUsuario();
        return instancia;
    }



}
