package edu.kinal.org.peliculakinal;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by familia on 25/07/15.
 */
public interface PeliculaService {
    @GET("/Peliculas")
    void getPeliculas(Callback<List<Peliculas>> callback);

}